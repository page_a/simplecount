package com.epitech.simplecount.controllers.functions;

import com.epitech.simplecount.models.Calculator;
import com.epitech.simplecount.controllers.AConfig;
import com.epitech.simplecount.models.CalculatorException;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by alex on 11/17/15.
 */
public class Exponential extends AConfig implements ActionListener
{
    public Exponential(Calculator calculator)
    {
        super(calculator);
    }

    public void actionPerformed(ActionEvent event)
    {
        try
        {
            c.setOperator("exp");
            c.calcul();
            c.setUpdate(true);
        }
        catch (CalculatorException e)
        {
            System.out.println(e.getMessage());
        }
    }
}
