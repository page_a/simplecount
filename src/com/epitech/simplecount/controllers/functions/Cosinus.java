package com.epitech.simplecount.controllers.functions;

import com.epitech.simplecount.models.Calculator;
import com.epitech.simplecount.controllers.AConfig;
import com.epitech.simplecount.models.CalculatorException;
import com.epitech.simplecount.views.Label;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.math.BigDecimal;

/**
 * Created by alex on 11/17/15.
 */
public class Cosinus extends AConfig implements ActionListener
{
    public Cosinus(Calculator calculator)
    {
        super(calculator);
    }

    public void actionPerformed(ActionEvent event)
    {
        try
        {
            c.setOperator("cos");
            c.calcul();
            c.setUpdate(true);
        }
        catch (CalculatorException e)
        {
            System.out.println(e.getMessage());
        }
    }
}
