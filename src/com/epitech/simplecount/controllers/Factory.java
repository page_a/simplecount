package com.epitech.simplecount.controllers;

import com.epitech.simplecount.models.Calculator;

import java.lang.reflect.Constructor;
import java.util.Arrays;

/**
 * Created by alex on 11/9/15.
 *
 * Factory class. This class is call to instanciate the appropriate class to the good ActionListener.
 * It make the link between the name of the button and the name of the class using reflectivity.
 *
 */
public class Factory
{
    private static final String[] number = { "0",  "1", "2", "3", "4", "5", "6", "7", "8", "9", "." };
    private static final String[] operators = { "=", "+", "-", "x", "/", "AC", "%"};
    private static final String[] function = {"\u221A", "cos", "sin", "tan", "x\u00B2", "log", "exp" };
    private static final String[] operatorClassNameModel = {"Equal", "Addition", "Substraction", "Multiply", "Division", "Reset", "Modulo"};
    private static final String[] functionsClassNameModel = {"SquareRoot", "Cosinus", "Sinus", "Tangent", "Square", "Logarithm",  "Exponential" };

    /** "Main" function call to return the good instancied class */
    public static Object getGoodClass(String key, Calculator calculator)
    {
        String className = Factory.getClassName(key);

        try
        {
            Constructor c = Class.forName("com.epitech.simplecount.controllers." + className).getConstructor(Calculator.class);
            Object o =  c.newInstance(calculator);
            return o;
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return null;
    }

    /** Search the appropriate name class corresponding to the key */
    private static String getClassName(String key)
    {
        String str = "";
        boolean isNumber = Arrays.asList(number).contains(key);
        boolean isOperator = Arrays.asList(operators).contains(key);
        boolean isFunction = Arrays.asList(function).contains(key);

        if (isNumber)
            str = "Numbers";
        else if (isOperator)
            str = Factory.getOperatorClassName(key);
        else if (isFunction)
            str = Factory.getFunctionClassName(key);
        return str;
    }

    /** Return the corresponding operator class name for key */
    private static String getOperatorClassName(String str)
    {
        int index = Arrays.asList(operators).indexOf(str);
        return "operators." + operatorClassNameModel[index];
    }

    /** Return the corresponding function class name for key */
    private static String getFunctionClassName(String str)
    {
        int index = Arrays.asList(function).indexOf(str);
        return "functions." + functionsClassNameModel[index];
    }
}
