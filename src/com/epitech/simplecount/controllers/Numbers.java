package com.epitech.simplecount.controllers;

import com.epitech.simplecount.models.Calculator;
import com.epitech.simplecount.views.Button;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by alex on 11/7/15.
 *
 * Number class. Called when user presser a figure
 *
 */
public class Numbers extends AConfig implements ActionListener
{
    public Numbers(Calculator calculator)
    {
        super(calculator);
    }

    /** Notify the view and set the number in the calculator */
    public void actionPerformed(ActionEvent e)
    {
        String str = c.getSecondOperand();
        String number = ((Button)e.getSource()).getText();

        if (c.getUpdate()) /** First time number is pressed */
            c.setUpdate(false);
        else /** Second time -> Concatenation with the precedent number */
        {
            if (!str.equals("0"))
                number = str + number;
            else if (str.equals("0") && number.equals("."))
                number = str + number;
        }
        c.setSecondOperand(number);
        c.notify(number);
    }
}
