package com.epitech.simplecount.controllers;

import com.epitech.simplecount.models.Calculator;

import java.awt.event.ActionListener;

/**
 * Created by alex on 11/17/15.
 *
 * Abstract class for each controller. Allow to get access the calculator.
 *
 */
public abstract class AConfig
{
    protected Calculator c;

    public AConfig(Calculator calculator)
    {
        this.c = calculator;
    }
}
