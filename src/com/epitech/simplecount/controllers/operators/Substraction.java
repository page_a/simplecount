package com.epitech.simplecount.controllers.operators;

import com.epitech.simplecount.models.Calculator;
import com.epitech.simplecount.controllers.AConfig;
import com.epitech.simplecount.models.CalculatorException;
import com.epitech.simplecount.views.Label;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.math.BigDecimal;

/**
 * Created by alex on 11/10/15.
 *
 *  * Substraction class. getOperation() is there for knowing if we have to calcul before making the substraction.
 *
 * The Update boolean is here to indicate that after pressed the + Button, we're attending a new Number
 * (And not concatening the first one with the next)
 */
public class Substraction extends AConfig implements ActionListener
{
    public Substraction(Calculator calculator)
    {
        super(calculator);
    }

    public void actionPerformed(ActionEvent event)
    {
        try
        {
            BigDecimal currentNumber = new BigDecimal(c.getSecondOperand());
            if (c.getOperation())
                c.calcul();
            else
            {
                c.setFirstOperand(currentNumber);
                c.setOperation(true);
            }
            c.setOperator("-");
            c.setUpdate(true);
        }
        catch (CalculatorException e)
        {
            System.out.println(e.getMessage());
        }
        catch (NumberFormatException e)
        {
            System.out.println(e.getMessage());
        }
    }
}
