package com.epitech.simplecount.controllers.operators;

import com.epitech.simplecount.models.Calculator;
import com.epitech.simplecount.controllers.AConfig;
import com.epitech.simplecount.models.CalculatorException;
import com.epitech.simplecount.views.Label;
import com.sun.corba.se.impl.protocol.AddressingDispositionException;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.math.BigDecimal;

/**
 * Created by alex on 11/10/15.
 *
 * Addition class. getOperation() is there for knowing if we have to calcul before making the addition.
 * Exemple : 5 + 6 + 3 -> 11 + 3 -> 14
 *
 * The Update boolean is here to indicate that after pressed the + Button, we're attending a new Number
 * (And not concatening the first one with the next)
 *
 */
public class Addition extends AConfig implements ActionListener
{
    public Addition(Calculator calculator)
    {
        super(calculator);
    }

    public void actionPerformed(ActionEvent event)
    {
        try
        {
            BigDecimal currentNumber = new BigDecimal(c.getSecondOperand());
            if (c.getOperation()) /** Already an Operator to execute ? Calcul then */
                c.calcul();
            else /** Set the firstOperand */
            {
                c.setFirstOperand(currentNumber);
                c.setOperation(true);
            }
            c.setOperator("+");
            c.setUpdate(true); /** See at the top */
        }
        catch (CalculatorException e)
        {
            System.out.println(e.getMessage());
        }
        catch (NumberFormatException e)
        {
            System.out.println(e.getMessage());
        }
    }
}
