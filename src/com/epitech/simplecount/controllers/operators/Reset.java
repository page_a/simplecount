package com.epitech.simplecount.controllers.operators;

import com.epitech.simplecount.models.Calculator;
import com.epitech.simplecount.controllers.AConfig;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by alex on 11/10/15.
 *
 *  Reset class.
 *
 */
public class Reset extends AConfig implements ActionListener
{
    public Reset(Calculator calculator)
    {
        super(calculator);
    }

    public void actionPerformed(ActionEvent e)
    {
        c.notify("0");
        c.reset();
        c.setUpdate(false); /** Waiting for a new Number after resetting. */
        c.setOperation(false); /** Next time we don't need to calcul, just set the firstOPerand number */
    }
}
