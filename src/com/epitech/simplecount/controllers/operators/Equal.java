package com.epitech.simplecount.controllers.operators;

import com.epitech.simplecount.models.Calculator;
import com.epitech.simplecount.controllers.AConfig;
import com.epitech.simplecount.models.CalculatorException;
import com.epitech.simplecount.views.Label;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.math.BigDecimal;

/**
 * Created by alex on 11/10/15.
 * Class equal. His only role is to calcul.
 *
 * The Update boolean is here to indicate that after pressed the + Button, we're attending a new Number
 * (And not concatening the first one with the next)
 *
 * The Operation boolean is here to indicate that we don't need to calcul something using the firstOperand and the
 * secondOperand when we will next pressing the operator's button.
 *
 */
public class Equal extends AConfig implements ActionListener
{
    public Equal(Calculator calculator)
    {
        super(calculator);
    }


    public void actionPerformed(ActionEvent event)
    {
        try
        {
            c.calcul();
            c.setUpdate(true); /** See at the top */
            c.setOperation(true); /** Same */
        }
        catch (CalculatorException e)
        {
            System.out.println(e.getMessage());
        }
        catch (NumberFormatException e)
        {
            System.out.println(e.getMessage());
        }
    }
}
