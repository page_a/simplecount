package com.epitech.simplecount.views;

/**
 * Created by alex on 11/5/15.
 *
 * Enum for customizing Button's color
 *
 */
public enum  Color
{
    ORANGE ("#f99011"),
    GREY ("#d5d6da"),
    DARK_GREY ("#c5c4c9"),
    SPECIAL_BLACK ("#202020");

    private String color;

    Color(String color)
    {
        this.color = color;
    }

    public String toString()
    {
        return color;
    }
}
