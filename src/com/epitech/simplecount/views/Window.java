package com.epitech.simplecount.views;

import com.epitech.simplecount.models.Calculator;
import com.epitech.simplecount.controllers.Factory;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.util.Arrays;
import java.util.Observable;
import java.util.Observer;

/**
 * Created by alex on 11/4/15.
 * This is the main View class
 */

public class Window extends JFrame implements Observer
{
    private JPanel buttons = new JPanel(new GridBagLayout());
    private JPanel resultField = new JPanel(new GridBagLayout());
    private JPanel container = new JPanel();
    private Button[] tab_buttons;
    private GridBagConstraints gbc = new GridBagConstraints();
    private Label result = new Label("0");
    private static final String[] key = {"log", "exp", "0", ".", "=", "x\u00B2", "1", "2", "3", "+", "tan", "4", "5", "6", "-", "sin", "7", "8", "9", "x", "cos", "AC", "\u221A", "%", "/" };
    private static final String[] operators = {"=", "+", "-", "x", "/"};
    private static final String[] specialTouch = {"AC", "+/-", "%", "/", "cos", "\u221A"};
    private Calculator c;

    /** Constructor calling Calculators class and Initializing Button */
    public Window()
    {
        this.setTitle("SimpleCount");
        this.setSize(550, 650);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLocationRelativeTo(null);
        this.setResizable(false);

        c = new Calculator(this);
        initButtons();

        this.setContentPane(container);
        this.setVisible(true);
    }

    /** This function initialize all the button in the view. It also call the Static
     * Factory class that use reflectivity to Instance the good Controller class to each button. (See Factory class in Controller's folder)
     */
    public void initButtons()
    {
        gbc.fill = GridBagConstraints.BOTH;
        gbc.weightx = 25;
        gbc.weighty = 25;
        gbc.anchor = GridBagConstraints.NORTHWEST;
        gbc.insets = new Insets(0, 0, 0, 0);
        tab_buttons = new Button[key.length];

        for (int i = 0; i < tab_buttons.length; i++) /** Building button and setting up the actionListener */
        {
            if (Arrays.asList(operators).contains(key[i]))
                tab_buttons[i] = new Button(key[i], Color.ORANGE.toString(), java.awt.Color.WHITE);
            else if (Arrays.asList(specialTouch).contains(key[i]))
                tab_buttons[i] = new Button(key[i], Color.DARK_GREY.toString(), java.awt.Color.BLACK);
            else
                tab_buttons[i] = new Button(key[i]);
            tab_buttons[i].addActionListener((ActionListener) (Factory.getGoodClass(key[i], c))); /** Using Factory class to get the appropriate class */
        }


        int start = 0;
        for (int y = 5; y > 0; y--)
        {
            for (int x = 0; x < 5; x++)
            {
                gbc.gridx = x;
                gbc.gridy = y;
                buttons.add(tab_buttons[start], gbc);
                start++;
            }
        }


        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.gridwidth = 5;
        resultField.setPreferredSize(new Dimension(550, 120));
        GridBagConstraints g = new GridBagConstraints();
        g.fill = GridBagConstraints.NONE;
        g.anchor = GridBagConstraints.SOUTHEAST;
        g.weightx = 25;
        g.weighty = 25;
        g.gridx = 0;
        g.gridy = 0;
        resultField.add(result, g);

        resultField.setBackground(java.awt.Color.decode(Color.SPECIAL_BLACK.toString()));
        resultField.setOpaque(true);
        buttons.add(resultField, gbc);

        container.setBackground(java.awt.Color.decode(Color.SPECIAL_BLACK.toString()));
        container.setOpaque(true);

        container.add(buttons);
    }

    /** Method call when The model (calculator) notify the changed of the result to display */
    public void update(Observable obj, Object x)
    {
        result.setText((String)x);
    }

}
