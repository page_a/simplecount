package com.epitech.simplecount.views;

import com.epitech.simplecount.views.Color;

import javax.swing.*;
import java.awt.Font;

/**
 * Created by alex on 11/6/15.
 *
 * Custom Label extending JLabel with specific values such
 * as Police or Color
 *
 */
public class Label extends JLabel
{
    public Label(String title)
    {
        this.setText(title);
        this.setFont(new Font("SFNS Display Thin", Font.PLAIN, 70));
        this.setForeground(java.awt.Color.WHITE);
        this.setBackground(java.awt.Color.decode(Color.SPECIAL_BLACK.toString()));
        this.setOpaque(true);
    }
}
