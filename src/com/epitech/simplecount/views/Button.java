package com.epitech.simplecount.views;

import javax.swing.JButton;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;
import java.awt.*;
import java.awt.Color;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

/**
 * Created by alex on 11/5/15.
 *
 * Custom class Button extending JButton with specific attribute such as the police, the color.
 *
 */
public class Button extends JButton
{
    public Button(String title)
    {
        super(title);
        this.commonInit(title);
        this.setBackground(Color.decode("#d5d6da"));
        this.setForeground(Color.BLACK);
    }

    /** Second constructor, with color, and FontColor param */
    public Button(String title, String color, Color fontColor)
    {
        super(title);
        this.commonInit(title);
        this.setBackground(java.awt.Color.decode(color));
        this.setForeground(fontColor);
    }

    /** Common initialization to all button (Border black, Police) */
    private void commonInit(String title)
    {
        this.setText(title);
        this.setBorder(new LineBorder(Color.BLACK, 1));
        this.setFont(new Font("SFNS Display Thin", Font.BOLD, 30));
        this.setPreferredSize(new Dimension(100, 100));
    }
}
