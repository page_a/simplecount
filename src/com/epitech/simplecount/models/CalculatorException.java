package com.epitech.simplecount.models;

/**
 * Created by alex on 11/18/15.
 */
public class CalculatorException extends Exception
{
    public CalculatorException()
    {
        super();
    }

    public CalculatorException(String message)
    {
        super(message);
    }
}
