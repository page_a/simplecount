package com.epitech.simplecount.models;

import com.epitech.simplecount.views.Label;
import com.epitech.simplecount.views.Window;

import javax.script.ScriptEngineManager;
import javax.swing.event.EventListenerList;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.EventListener;
import java.util.Observable;

/**
 * Created by alex on 11/10/15.
 *
 * This is the calculator class, the one which make calcul. It extends Observable to notify
 * the view that the result has changed and that need to be updated.
 *
 */
public class Calculator extends Observable
{
    private BigDecimal firstOperand;
    private String secondOperand;
    private String operator = "";
    private Boolean operation = false;
    private Boolean update = false;

    public Calculator(Window window)
    {
        this.firstOperand = new BigDecimal("0");
        this.secondOperand = new String("0");
        addObserver(window);
    }

    /**   Main function which calcul the result based on the firstOperend and the secondOperand */
    public void calcul()
            throws CalculatorException
    {
        BigDecimal secondOp = new BigDecimal(getSecondOperand()).setScale(8, BigDecimal.ROUND_HALF_UP);
        String precision = null;

        try
        {
            switch (operator)
            {
                case "+":
                    setFirstOperand(firstOperand.add(secondOp));
                    break;
                case "-":
                    setFirstOperand(firstOperand.subtract(secondOp));
                    break;
                case "*":
                    setFirstOperand(firstOperand.multiply(secondOp));
                    break;
                case "/":
                {
                    setFirstOperand(firstOperand.divide(secondOp, BigDecimal.ROUND_HALF_UP));
                    break;
                }
                case "%":
                    setFirstOperand(firstOperand.remainder(secondOp));
                    break;
                case "exp":
                {
                    BigDecimal result = new BigDecimal(Math.exp(secondOp.doubleValue()));
                    setFirstOperand(result);
                    break;
                }
                case "square":
                    BigDecimal result = new BigDecimal(Math.pow(secondOp.doubleValue(), 2));
                    setFirstOperand(result);
                    break;
                case "squareRoot":
                    result = new BigDecimal(Math.sqrt(secondOp.doubleValue()));
                    setFirstOperand(result);
                    break;
                case "log":
                    result = new BigDecimal(Math.log(secondOp.doubleValue()));
                    setFirstOperand(result);
                    break;
                case "cos":
                case "sin":
                case "tan":
                {
                    BigDecimal pi = new BigDecimal(Math.PI);
                    BigDecimal rad = secondOp.multiply(pi.divide(new BigDecimal("180"), BigDecimal.ROUND_UP));
                    result = new BigDecimal("0");
                    switch (operator)
                    {
                        case "cos":
                            result = new BigDecimal(Math.cos(rad.doubleValue()));
                            break;
                        case "sin":
                            result = new BigDecimal(Math.sin(rad.doubleValue()));
                            break;
                        case "tan":
                            result = new BigDecimal(Math.tan(rad.doubleValue()));
                            break;
                    }
                    setFirstOperand(result);
                    break;
                }
                case "":
                    setFirstOperand(secondOp);
                    break;
            }
            precision = makePrecision(getFirstOperand());
        }
        catch (StringIndexOutOfBoundsException e)
        {
            precision = "0";
        }
        catch (ArithmeticException e)
        {
            reset();
            throw new CalculatorException("Div per 0");
        }
        catch (NumberFormatException e)
        {
            precision = "Error";
        }
        setSecondOperand(getFirstOperand().toString());
        notify(precision);
        this.setOperator("");
    }

    /** Notify function which call the two function of class Observable needed to notify the view */
    public void notify(String str)
    {
        setChanged();
        notifyObservers(str);
    }

    /** Reset of all number of the calculator */
    public void reset()
    {
        setFirstOperand(new BigDecimal("0"));
        setSecondOperand("0");
    }

    /** Function returning String in scientific notation */
    private static String format(BigDecimal x, int scale)
    {
        NumberFormat formatter = new DecimalFormat("0.0E0");
        formatter.setRoundingMode(RoundingMode.HALF_UP);
        formatter.setMinimumFractionDigits(scale);
        return formatter.format(x);
    }

    /** Precision function */
    private String makePrecision(BigDecimal nb2)
    {
        String number = String.valueOf(nb2);
        boolean a = false;
        byte precision = 0;
        int before = 0;

        for (int i = 0; i < number.length(); i++) /** Counting number of figure */
        {
            if (!a)
                before++;
            if (number.charAt(i) == '.')
                a = true;
            else if (a && number.charAt(i) != '0')
                precision++;
        }
        if ((precision + before) > 8) /** Size of entier number > 8 returning scientific notation with 8 figures */
            return format(nb2, 8);
        if (precision < 2) /** Minimum precision : 2 */
            precision = 2;
        else if (precision > 8) /** Mx precision : 8 */
            precision = 8;
        return number.substring(0, precision + before);
    }

    public void setSecondOperand(String str)
    {
        this.secondOperand = str;
    }

    public String getSecondOperand()
    {
        return this.secondOperand;
    }

    public boolean getOperation()
    {
        return this.operation;
    }

    public boolean getUpdate()
    {
        return this.update;
    }

    public void setUpdate(boolean b)
    {
        this.update = b;
    }

    public void setFirstOperand(BigDecimal nb2)
    {
        this.firstOperand = nb2.setScale(8, BigDecimal.ROUND_HALF_UP);
    }

    public BigDecimal getFirstOperand()
    {
        return this.firstOperand;
    }

    public void setOperator(String op)
    {
        this.operator = op;
    }

    public void setOperation(boolean b)
    {
        this.operation = b;
    }

    public String getOperator() { return this.operator; }
}
